# CSV Prepare

anpassen von spalten und inhalten einer csv.
Ohne Internet Verbindung in JavaScript.

# create executable .exe file
with pyinstaller
https://www.youtube.com/watch?v=UZX5kH72Yx4
https://datatofish.com/executable-pyinstaller/

# remove csv columns
https://nitratine.net/blog/post/remove-columns-in-a-csv-file-with-python/

# get started

## 1. python installieren
https://www.python.org/ftp/python/3.9.0/python-3.9.0-amd64.exe

## 2. tool ausführen
doppelklick auf die start.bat datei.
