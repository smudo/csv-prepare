#!/usr/bin/env python3
import csv
import fileinput
import os
import pathlib

# vars
cols_to_remove = [0, 1, 3, 4, 5, 6, 8, 10, 12, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30]
current_path = os.getcwd()

def getdelimeter(file):
    sample = open(file).read(2048)
    dialect = csv.Sniffer().sniff(sample)
    print("dialect: ", dialect.delimiter)
    return dialect.delimiter


def rmtempfiles():
    print("search and remove temp file")
    for file in os.listdir(current_path):
        filename = os.fsdecode(file)
        if filename.startswith("for gls_tempfile"):
            filename_string = os.path.join(current_path, filename)
            print("tempfile found:", filename_string)
            os.remove(filename_string)
            continue
        else:
            print("no temp file found")
            continue


def umlauteraus(filename_for_proccedere):
    data = ""
    with open(filename_for_proccedere, "r", encoding='utf-8') as file:
         data = file.read().replace("ü", "ue").replace("Ü", "Ue").replace(" ä","ae").replace(" Ä","Ae").replace(" ö","oe").replace(" Ö","Oe")

    finalfilename = filename_for_proccedere # z.B.: 'for gls_tempfile6_BAS_CZ.csv'
    with open(finalfilename, "w", newline='', encoding='utf-8') as file:
         file.write(data)



def createnewcsvfile(filename_for_proccedere, rows):
    temp_file6 = "for gls_"+filename_for_proccedere
    with open(temp_file6, "w", newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        for row in rows:
            writer.writerow(row)
    umlauteraus(temp_file6)


def splitcsv(filename_for_proccedere):
    print("split csv for Lieferanten: ", filename_for_proccedere)

    # file name's
    filename_Übertragung_an_lieferanten_Umarex = "for gls_lieferanten_Umarex.csv"
    filename_vorort_lagernd = "for gls_vorort_lagernd.csv"
    filename_Lieferant_BAS_CZE = "for gls_Lieferant_BAS_CZE.csv"
    filename_Lieferant_GFC_PL = "for gls_Lieferant_GFC_P.csv"
    filename_Lieferant_VE_NL_UK = "for gls_Lieferant_VE_NL_UK.csv"
    filename_Lieferant_AI_ESP = "for gls_Lieferant_AI_ESP.csv"
    filename_Lieferant_EVO_IT = "for gls_Lieferant_EVO_IT.csv"

    rows_an_lieferanten_Umarex = []
    rows_vorort_lagernd = []
    rows_Lieferant_BAS_CZ = []
    rows_Lieferant_GFC_PL = []
    rows_Lieferant_VE_NL_UK = []
    rows_Lieferant_AI_ESP = []
    rows_Lieferant_EVO_IT = []

    # Stati
    Übertragung_an_lieferanten_Umarex = "Fertig für den Versand"
    vorort_lagernd = "Deine Bestellung bei AirSoftArms.eu ist versandbereit"
    Lieferant_BAS_CZE = "Bestellung an Versandteam/Zentrallager CZ/Prag übertragen"
    Lieferant_GFC_PL = "Bestellung an Versandteam/Zentrallager PRE /PL übertragen"
    Lieferant_VE_NL_UK = "Bestellung an Versandteam/Zentrallager VE NL/GB übertragen"
    Lieferant_AI_ESP = "Bestellung an Versandteam/Zentrallager INT / ESP übertragen"
    Lieferant_EVO_IT = "Bestellung an Versandteam/Zentrallager IT/MIL übertragen"

    if os.path.exists(filename_for_proccedere):
        with open(filename_for_proccedere, "r", encoding='utf-8') as source:
            reader = csv.reader(source)
            for row in reader:
                print("row[12]: ", row[12])
                if row[12] == Übertragung_an_lieferanten_Umarex:
                    print("row[12] erkannt!: ", row[12])
                    rows_an_lieferanten_Umarex.append(row)
                    print("thats look the: ", rows_an_lieferanten_Umarex)
                if row[12] == vorort_lagernd:
                    print("row[12] erkannt!: ", row[12])
                    rows_vorort_lagernd.append(row)
                if row[12] == Lieferant_BAS_CZE:
                    print("row[12] erkannt!: ", row[12])
                    rows_Lieferant_BAS_CZ.append(row)
                if row[12] == Lieferant_GFC_PL:
                    print("row[12] erkannt!: ", row[12])
                    rows_Lieferant_GFC_PL.append(row)
                if row[12] == Lieferant_VE_NL_UK:
                    print("row[12] erkannt!: ", row[12])
                    rows_Lieferant_VE_NL_UK.append(row)
                if row[12] == Lieferant_AI_ESP:
                    print("row[12] erkannt!: ", row[12])
                    rows_Lieferant_AI_ESP.append(row)
                if row[12] == Lieferant_EVO_IT:
                    print("row[12] erkannt!: ", row[12])
                    rows_Lieferant_EVO_IT.append(row)
            if len(rows_an_lieferanten_Umarex) != 0:
                print("List is empty")
                createnewcsvfile("Umarex.csv", rows_an_lieferanten_Umarex)
            if len(rows_vorort_lagernd) != 0:
                print("List is empty")
                createnewcsvfile("vorort_lagernd.csv", rows_vorort_lagernd)
            if len(rows_Lieferant_BAS_CZ) != 0:
                print("List is empty")
                createnewcsvfile("BAS_CZ.csv", rows_Lieferant_BAS_CZ)
            if len(rows_Lieferant_GFC_PL) != 0:
                print("List is empty")
                createnewcsvfile("GFC_PL.csv", rows_Lieferant_GFC_PL)
            if len(rows_Lieferant_VE_NL_UK) != 0:
                print("List is empty")
                createnewcsvfile("VE_NL_UK.csv", rows_Lieferant_VE_NL_UK)
            if len(rows_Lieferant_AI_ESP) != 0:
                print("List is empty")
                createnewcsvfile("AI_ESP.csv", rows_Lieferant_AI_ESP)
            if len(rows_Lieferant_EVO_IT) != 0:
                print("List is empty")
                createnewcsvfile("EVO_IT.csv", rows_Lieferant_EVO_IT)


def convert(filename_for_proccedere):
    newname = "for gls_"+filename_for_proccedere
    temp_file = "for gls_tempfile_"+filename_for_proccedere
    temp_file2 = "for gls_tempfile2_"+filename_for_proccedere
    temp_file3 = "for gls_tempfile3_"+filename_for_proccedere
    temp_file4 = "for gls_tempfile4_"+filename_for_proccedere
    temp_file5 = "for gls_GESAMT_"+filename_for_proccedere
    print("beginning converting")
    # print("replace ';' & delete not nessecery columns")
    cols_to_remove = [0, 1, 3, 4, 5, 6, 8, 10, 12, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30]
    if os.path.exists(filename_for_proccedere):
        # replacing ; with ,
        print("replace ; with ,")
        with fileinput.FileInput(filename_for_proccedere, inplace=False, backup='.bak') as file:
            for line in file:
                print(line.replace(";", ","), end='')

        print("delete not nessecery columns")
        cols_to_remove = sorted(cols_to_remove, reverse=True)  # Reverse so we remove from the end first
        row_count = 0  # Current amount of rows processed

        with open(filename_for_proccedere, "r", encoding='utf-8') as source:
            reader = csv.reader(source)
            with open(temp_file, "w", newline='', encoding='utf-8') as result:
                writer = csv.writer(result)
                for row in reader:
                    row_count += 1
                    print('\r{0}'.format(row_count), end='')  # Print rows processed
                    for col_index in cols_to_remove:
                        del row[col_index]
                    writer.writerow(row)
    else:
        print("datei nicht gefunden")

    print("reordering ")
    if os.path.exists(filename_for_proccedere):

        print("reorder columns")
        with open(temp_file, 'r', encoding='utf-8') as infile, open(temp_file2, 'a', newline='', encoding='utf-8') as outfile:
            # output dict needs a list for new column ordering
            # writer = infile.replace(";", ",")
            oldfieldnames = ['Status', 'Shipping Address Attention', 'Shipping Address Address1', 'Shipping Address Number', 'Shipping Address Zipcode', 'Shipping Address City', 'Shipping Address Country', 'Phone', 'Email']
            fieldnames = ['Shipping Address Attention', 'Shipping Address Address1', 'Shipping Address Country', 'Shipping Address Zipcode', 'Shipping Address City', 'Phone', 'Email', 'Status', 'Shipping Address Number']
            # empfänger-ID;Name;;;Straße;;Länderkürzel;PLZ;Stadt;;Telefon;Email;;;;;;Hausnummer
            # 12;MustermannMax;;;Musterstraße;;de;36286;Neuenstein;;;;;;;;;3
            print("write new file")
            writer = csv.DictWriter(outfile, fieldnames=fieldnames, delimiter=';')
            # reorder the header first
            writer.writeheader()
            print("infile:", infile)
            for row in csv.DictReader(infile):
                # writes the reordered rows to the new file
                writer.writerow(row)
            print("Done")
    else:
        print("The file does not exist")

    print("fill in missing columns for your-gls")
    default_text = ''
    # Open the input_file in read mode and output_file in write mode
    with open(temp_file2, 'r', encoding='utf-8') as read_obj, open(temp_file3, 'w', newline='', encoding='utf-8') as write_obj: #  --> newline https://docs.python.org/3/library/csv.html#csv.reader
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj, delimiter=';')
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj)
        # Read each row of the input csv file as list
        for row in csv_reader:
            # Append the default text in the row / list
            # row.append(default_text)
            row.insert(0, default_text) # empfänger ID
            row.insert(2, default_text) # name2
            row.insert(3, default_text) # name3
            row.insert(5, default_text) # matchcode
            row.insert(9, default_text) # Kontaktperson
            row.insert(13, default_text) # Telefon-Ländervorwahl
            row.insert(14, default_text) # Telefon-Vorwahl
            row.insert(15, default_text) # Telefonnummer
            row.insert(16, default_text) # Telefon-Durchwahl
            # Add the updated row / list to the output file
            csv_writer.writerow(row)

    print("cut out header")
    # Open the input_file in read mode and output_file in write mode
    with open(temp_file3,'r', encoding='utf-8') as f, open(temp_file4,'w', encoding='utf-8') as f1:
        next(f) # skip header line
        for line in f:
            f1.write(line)

# https://www.kite.com/python/answers/how-to-check-if-a-string-contains-a-number-in-python
    print("move value from adress to number (last pos.)")
    # TODO: detect street number from streetname column and transfer to streetnumber clumn
    with open(temp_file4, 'r', encoding='utf-8') as read_obj, open(temp_file5, 'w', newline='', encoding='utf-8') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj, delimiter=',')
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj)
        n = 0
        # Read each row of the input csv file as list
        for row in csv_reader:
            n = n + 1
            print("here comes row", n)
            print("row type: ", type(row))
            print("row[4] type: ", type(row[0]))
            # Append the default text in the row / list
            # row.append(default_text)
            if any(map(str.isdigit, row[4])) is True :
                print("found digit in row[4]")
                print("move digit to the last column.")
                # move string from row[4] -> row[17]
                row[17] = row[4] # Straßennummer
                # row.insert(17, row[4]) # Straßennummer
                # delete digits from row[4]
                no_digits = []
                for i in row[4]:
                    if not i.isdigit():
                        no_digits.append(i)
                row[4] = ''.join(no_digits)
                # delete whitespaces from row[4]
                withspaces = row[4]
                withoutspaces = withspaces.lstrip(' ')
                row[4] = withoutspaces
                # delete none digits from row[17]
                digits = []
                for i in row[17]:
                    if i.isdigit():
                        digits.append(i)
                row[17] = ''.join(digits)

            # Add the updated row / list to the output file
            csv_writer.writerow(row)

    splitcsv(temp_file5)


for file in os.listdir(current_path):
    filename = os.fsdecode(file)
    if filename.endswith(".csv") and not filename.startswith("for gls_"):
        filename_string = os.path.join(current_path, filename)
        print("Datei:", filename_string)
        # check if its an export and if yes than procced with converting
        with open(filename) as f:
            first_line = f.readline()
            if first_line[0:8] == 'Shipment':
                # the proccedere
                print("von litghespeed!!")
                convert(filename)
                rmtempfiles()
            else:
                print("nicht richtige Datei ")
        continue
    else:
        if filename.endswith(".csv"):
            print("keine litghespeed Datei:", filename)
        continue
