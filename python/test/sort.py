#!/usr/bin/env python3
# Version 1.5
import csv
import fileinput
import os
import pathlib

# vars
current_path = os.getcwd()
filenumber = 0


def validcsv(file):  # TODO: csv-validator
    print("[+] checking csv validation: ", file)
    valid = True
    brokenlines = []
    correctlines = []
    brokenline_filename = 'nicht erkannte Zeilen ' + file
    with open(file, 'r', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=';')
        for line in reader:
            # print(len(line))
            if len(line) == 61 or len(line) == 31:
                correctlines.append(line)
                # print("correctlines: ", line)
            else:
                valid = False
                brokenlines.append(line)
                # print("inncorrect: ", line)
        if brokenlines:
            createnewcsvfile(brokenline_filename, brokenlines, praefix=False)
        if correctlines:
            filename = "for gls_tempfile-valid " + file
            createnewcsvfile(filename, correctlines, praefix=False)

        if valid is True:
            filename = file
            print("[+] file is valid! ", file)
        else:
            print("[+] file is NOT valid! ", file)
            print("[+] not valid lines in file: ", file)
            print("[+] new filename: ", filename)
    ## replace tabs (\t)
    # #input file
    # fin = open(filename, "rt")
    # #output file to write the result to
    # fout = open(filename, "wt")
    # #for each line in the input file
    # for line in fin:
    #     #read replace the string and write to output file
    #     fout.write(line.replace('hfsgivkdfhg', ' '))
    # #close input and output files
    # fin.close()
    # fout.close()
    return filename  # for gls_tempfile2_not-valid orders-export-2021-01-12-14-11-53.csv


def rmtempfiles():
    print("search and remove temp file")
    for file in os.listdir(current_path):
        filename = os.fsdecode(file)
        if filename.startswith("for gls_tempfile"):
            filename_string = os.path.join(current_path, filename)
            print("tempfile found:", filename_string)
            os.remove(filename_string)
            continue
        else:
            # print("no temp file found")
            continue


def getdelimeter(file):
    sample = open(file).read(8192)
    dialect = csv.Sniffer().sniff(sample)
    print("dialect: ", dialect.delimiter)
    return dialect.delimiter


def sonderzeichenraus(filename_for_proccedere):
    data = ""
    with open(filename_for_proccedere, "r", encoding='utf-8') as file:
         data = file.read().replace("ü", "ue").replace("Ü", "Ue").replace("ä","ae").replace("Ä","Ae").replace("ö","oe").replace("Ö","Oe").replace("\"", "")

    finalfilename = filename_for_proccedere # z.B.: 'for gls_tempfile6_BAS_CZ.csv'
    with open(finalfilename, "w", newline='', encoding='utf-8') as file:
         file.write(data)




def createnewcsvfile(filename_for_proccedere, rows, praefix):
    if praefix == True:
        temp_file6 = "for gls_"+filename_for_proccedere
        print("[+] create new file with präfix: ", temp_file6)
    else:
        temp_file6 = filename_for_proccedere
        print("[+] create new file without präfix: ", filename_for_proccedere)
    with open(temp_file6, "w", newline='', encoding='utf-8') as file:
        writer = csv.writer(file, delimiter=';')
        for row in rows:
            writer.writerow(row)


def splitcsv(filename_for_proccedere, originalname, lspeedversion):
    print("split csv for Lieferanten: ", filename_for_proccedere)
    print("[-] ORIGNALNAME: ", originalname)
    # file name's
    filename_Übertragung_an_lieferanten_Umarex = "lieferanten_Umarex - " + originalname
    filename_vorort_lagernd = "vorort_lagernd - " + originalname
    filename_Lieferant_BAS_CZE = "Lieferant_BAS_CZE - " + originalname
    filename_Lieferant_GFC_PL = "Lieferant_GFC_P - " + originalname
    filename_Lieferant_VE_NL_UK = "Lieferant_VE_NL_UK - " + originalname
    filename_Lieferant_AI_ESP = "Lieferant_AI_ESP - " + originalname
    filename_Lieferant_EVO_IT = "Lieferant_EVO_IT - " + originalname

    rows_an_lieferanten_Umarex = []
    rows_vorort_lagernd = []
    rows_Lieferant_BAS_CZ = []
    rows_Lieferant_GFC_PL = []
    rows_Lieferant_VE_NL_UK = []
    rows_Lieferant_AI_ESP = []
    rows_Lieferant_EVO_IT = []

    # Stati
    Übertragung_an_lieferanten_Umarex = "Fertig für den Versand"
    vorort_lagernd = "Deine Bestellung bei AirSoftArms.eu ist versandbereit"
    Lieferant_BAS_CZE = "Bestellung an Versandteam/Zentrallager CZ/Prag uebertragen"
    Lieferant_GFC_PL = "Bestellung an Versandteam/Zentrallager PRE /PL uebertragen"
    Lieferant_VE_NL_UK = "Bestellung an Versandteam/Zentrallager VE NL/GB uebertragen"
    Lieferant_AI_ESP = "Bestellung an Versandteam/Zentrallager INT / ESP uebertragen"
    Lieferant_EVO_IT = "Bestellung an Versandteam/Zentrallager IT/MIL uebertragen"
    #Bestellung an Zentrallager AT1 KUF uebertragen

    if os.path.exists(filename_for_proccedere):
        with open(filename_for_proccedere, "r", encoding='utf-8') as source:
            reader = csv.reader(source, delimiter=';')
            if lspeedversion == 0:
                for row in reader:
                    # print("row[12]: ", row[12])
                    if row[12] == Übertragung_an_lieferanten_Umarex:
                        # print("row[12] erkannt!: ", row[12])
                        rows_an_lieferanten_Umarex.append(row)
                        # print("thats look the: ", rows_an_lieferanten_Umarex)
                    if row[12] == vorort_lagernd:
                        # print("row[12] erkannt!: ", row[12])
                        rows_vorort_lagernd.append(row)
                    if row[12] == Lieferant_BAS_CZE:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_BAS_CZ.append(row)
                    if row[12] == Lieferant_GFC_PL:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_GFC_PL.append(row)
                    if row[12] == Lieferant_VE_NL_UK:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_VE_NL_UK.append(row)
                    if row[12] == Lieferant_AI_ESP:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_AI_ESP.append(row)
                    if row[12] == Lieferant_EVO_IT:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_EVO_IT.append(row)
                if len(rows_an_lieferanten_Umarex) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Übertragung_an_lieferanten_Umarex, rows_an_lieferanten_Umarex, True)
                if len(rows_vorort_lagernd) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_vorort_lagernd, rows_vorort_lagernd, True)
                if len(rows_Lieferant_BAS_CZ) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Lieferant_BAS_CZE, rows_Lieferant_BAS_CZ, True)
                if len(rows_Lieferant_GFC_PL) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Lieferant_GFC_PL, rows_Lieferant_GFC_PL, True)
                if len(rows_Lieferant_VE_NL_UK) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Lieferant_VE_NL_UK, rows_Lieferant_VE_NL_UK, True)
                if len(rows_Lieferant_AI_ESP) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Lieferant_AI_ESP, rows_Lieferant_AI_ESP, True)
                if len(rows_Lieferant_EVO_IT) != 0:
                    # print("List is empty")
                    createnewcsvfile(filename_Lieferant_EVO_IT, rows_Lieferant_EVO_IT, True)
            elif lspeedversion == 1:
                for row in reader:
                    # print("row[12]: ", row[12])
                    if row[12] == Übertragung_an_lieferanten_Umarex:
                        # print("row[12] erkannt!: ", row[12])
                        rows_an_lieferanten_Umarex.append(row)
                        # print("thats the: ", rows_an_lieferanten_Umarex)
                    if row[12] == vorort_lagernd:
                        # print("row[12] erkannt!: ", row[12])
                        # print("vorort lagernd")
                        rows_vorort_lagernd.append(row)
                    if row[12] == Lieferant_BAS_CZE:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_BAS_CZ.append(row)
                    if row[12] == Lieferant_GFC_PL:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_GFC_PL.append(row)
                    if row[12] == Lieferant_VE_NL_UK:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_VE_NL_UK.append(row)
                    if row[12] == Lieferant_AI_ESP:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_AI_ESP.append(row)
                    if row[12] == Lieferant_EVO_IT:
                        # print("row[12] erkannt!: ", row[12])
                        rows_Lieferant_EVO_IT.append(row)
                if len(rows_an_lieferanten_Umarex) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Übertragung_an_lieferanten_Umarex, rows_an_lieferanten_Umarex, True)
                if len(rows_vorort_lagernd) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_vorort_lagernd, rows_vorort_lagernd, True)
                if len(rows_Lieferant_BAS_CZ) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Lieferant_BAS_CZE, rows_Lieferant_BAS_CZ, True)
                if len(rows_Lieferant_GFC_PL) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Lieferant_GFC_PL, rows_Lieferant_GFC_PL, True)
                if len(rows_Lieferant_VE_NL_UK) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Lieferant_VE_NL_UK, rows_Lieferant_VE_NL_UK, True)
                if len(rows_Lieferant_AI_ESP) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Lieferant_AI_ESP, rows_Lieferant_AI_ESP, True)
                if len(rows_Lieferant_EVO_IT) != 0:
                    print("List is empty")
                    createnewcsvfile(filename_Lieferant_EVO_IT, rows_Lieferant_EVO_IT, True)


def convert(filename_for_proccedere, lspeedversion):
    originalname = filename_for_proccedere
    newname = "for gls_"+filename_for_proccedere
    temp_file = "for gls_tempfile_"+filename_for_proccedere
    temp_file2 = "for gls_tempfile2_"+filename_for_proccedere
    temp_file3 = "for gls_tempfile3_"+filename_for_proccedere
    temp_file4 = "for gls_tempfile4_"+filename_for_proccedere
    temp_file5 = "for gls_GESAMT_"+filename_for_proccedere
    print("beginning converting, file: ", originalname)
    # print("replace ';' & delete not nessecery columns")
    if lspeedversion == 0: #shipment
        cols_to_remove = [0, 1, 3, 4, 5, 6, 8, 10, 12, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30]
    elif lspeedversion == 1: #orders
        cols_to_remove = [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 22, 24, 26, 28, 31, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60] # was neues

    if os.path.exists(filename_for_proccedere):
        # replacing ; with ,
        # print("replace ; with ,")
        # with fileinput.FileInput(filename_for_proccedere, inplace=False, backup='.bak', openhook=fileinput.hook_encoded("utf-8")) as file:
        #     for line in file:
        #         print(line.replace(";", ","), end='')

        print("delete not nessecery columns")
        cols_to_remove = sorted(cols_to_remove, reverse=True)  # Reverse so we remove from the end first
        row_count = 0  # Current amount of rows processed

        # TODO: handle excaption for not consistant rows
        with open(filename_for_proccedere, "r", encoding='utf-8') as source:
            reader = csv.reader(source, delimiter=";")
            with open(temp_file, "w", newline='', encoding='utf-8') as result:
                writer = csv.writer(result, delimiter=',')
                for row in reader:
                    row_count += 1
                    # print('\r{0}'.format(row_count), end='')  # Print rows processed
                    for col_index in cols_to_remove:
                        #print("cols_to_remove: ", cols_to_remove)
                        # print("col_index: ", col_index)
                        del row[col_index]
                    writer.writerow(row)
    else:
        print("datei nicht gefunden")

    print("reordering")
    if os.path.exists(filename_for_proccedere):

        print("reorder columns")
        with open(temp_file, 'r', encoding='utf-8') as infile, open(temp_file2, 'a', newline='', encoding='utf-8') as outfile:
            # output dict needs a list for new column ordering
            # writer = infile.replace(";", ",")
            if lspeedversion == 0:
                oldfieldnames = ['Status', 'Shipping Address Attention', 'Shipping Address Address1', 'Shipping Address Number', 'Shipping Address Zipcode', 'Shipping Address City', 'Shipping Address Country', 'Phone', 'Email']
                fieldnames = ['Shipping Address Attention', 'Shipping Address Address1', 'Shipping Address Country', 'Shipping Address Zipcode', 'Shipping Address City', 'Phone', 'Email', 'Status', 'Shipping Address Number']
                # empfänger-ID;Name;;;Straße;;Länderkürzel;PLZ;Stadt;;Telefon;Email;;;;;;Hausnummer
            elif lspeedversion == 1:
                oldfieldnames = ['Custom_status', 'Firstname', 'Lastname', 'Phone', 'E-mail', 'Streetname', 'Number', 'Zipcode', 'City', 'Country']
                fieldnames = ['Firstname', 'Lastname', 'Streetname', 'Country', 'Zipcode', 'City', 'Phone', 'E-mail', 'Custom_status', 'Number']
            # empfänger-ID;Name;;;Straße;;Länderkürzel;PLZ;Stadt;;Telefon;Email;;;;;;Hausnummer
            # 12;MustermannMax;;;Musterstraße;;de;36286;Neuenstein;;;;;;;;;3
            print("write new file")
            writer = csv.DictWriter(outfile, fieldnames=fieldnames, delimiter=',')
            # reorder the header first
            writer.writeheader()
            print("infile:", infile)
            for row in csv.DictReader(infile):
                # writes the reordered rows to the new file
                writer.writerow(row)
            print("Done: ", filename_for_proccedere)
    else:
        print("The file does not exist")

    print("fill in missing columns for your-gls")
    default_text = ''
    # Open the input_file in read mode and output_file in write mode
    with open(temp_file2, 'r', encoding='utf-8') as read_obj, open(temp_file3, 'w', newline='', encoding='utf-8') as write_obj: #  --> newline https://docs.python.org/3/library/csv.html#csv.reader
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj, delimiter=',')
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj, delimiter=';')
        # Read each row of the input csv file as list
        for row in csv_reader:
            if lspeedversion == 0:
                # Append the default text in the row / list
                # row.append(default_text)
                row.insert(0, "") # empfänger ID
                row.insert(2, "") # name2
                row.insert(3, "") # name3
                row.insert(5, "") # matchcode
                row.insert(9, "") # Kontaktperson
                row.insert(13, "") # Telefon-Ländervorwahl
                row.insert(14, "") # Telefon-Vorwahl
                row.insert(15, "") # Telefonnummer
                row.insert(16, "") # Telefon-Durchwahl
                # Add the updated row / list to the output file

            if lspeedversion == 1:
                # fullname = row[1] + ' ' + row[2]
                row.insert(0, "") # empfänger ID
                row.insert(1, row[1] + ' ' + row[2]) # voller name
                row.insert(2, "") # name2 # TODO: copy fullname in this row too?
                row.insert(3, "") # name3
                row.insert(7, "") # matchcode
                row.insert(11, "") # Kontaktperson
                row.insert(15, "") # Telefon-Ländervorwahl
                row.insert(16, "") # Telefon-Vorwahl
                row.insert(15, "") # Telefonnummer
                row.insert(16, "") # Telefon-Durchwahl
                # Add the updated row / list to the output file
                # TODO: move first+last-name in row[1] and remove first row[4] and last name row[6]
                # print("row[4] :", row[4])
                # row[1] = row[4] + row[6]
                #
                del row[4] # del firstname
                del row[4] # del lastname
                # writer.writerow(row)
            csv_writer.writerow(row)
            # Print("bug")
    # if lspeedversion == 1:
    #     with open()
    #     row[1] = row[4] + row[6]
    #     # TODO: move first+last-name in row[1] and remove first row[4] and last name row[6]


    print("cut out header")
    # Open the input_file in read mode and output_file in write mode
    with open(temp_file3,'r', encoding='utf-8') as f, open(temp_file4,'w', encoding='utf-8') as f1:
        next(f) # skip header line
        for line in f:
            f1.write(line)

# https://www.kite.com/python/answers/how-to-check-if-a-string-contains-a-number-in-python
    print("move value from adress to number (last pos.)")
    # TODO: detect street number from streetname column and transfer to streetnumber clumn
    with open(temp_file4, 'r', encoding='utf-8') as read_obj, open(temp_file5, 'w', newline='', encoding='utf-8') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj, delimiter=';')
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj, delimiter=';')
        n = 0
        # Read each row of the input csv file as list
        for row in csv_reader:
            n = n + 1
            # print("here comes row", n)
            # print("row type: ", type(row))
            # print("row[4] type: ", type(row[0]))
            if lspeedversion == 0:
                # Append the default text in the row / list
                # row.append(default_text)
                if any(map(str.isdigit, row[4])) is True :
                    print("found digit in row[4]")
                    print("move digit to the last column.")
                    # move string from row[4] -> row[17]
                    row[17] = row[4] # Straßennummer
                    # row.insert(17, row[4]) # Straßennummer
                    # delete digits from row[4]
                    no_digits = []
                    for i in row[4]:
                        if not i.isdigit():
                            no_digits.append(i)
                    row[4] = ''.join(no_digits)

                    # delete whitespaces from row[4]
                    withspaces = row[4]
                    withoutspaces = withspaces.lstrip(' ')
                    row[4] = withoutspaces
                    # delete none digits from row[17]
                    digits = []
                    for i in row[17]:
                        if i.isdigit():
                            digits.append(i)
                    row[17] = ''.join(digits)
            elif lspeedversion == 1:
                # Append the default text in the row / list
                # row.append(default_text)
                if any(map(str.isdigit, row[4])) is True :
                    print("found digit in row[4]")
                    print("move digit to the last column.")
                    # move string from row[4] -> row[17]
                    row[17] = row[4] # Straßennummer
                    # row.insert(17, row[4]) # Straßennummer
                    # delete digits from row[4]
                    no_digits = []
                    for i in row[4]:
                        if not i.isdigit():
                            no_digits.append(i)
                    row[4] = ''.join(no_digits)

                    # delete whitespaces from row[4]
                    withspaces = row[4]
                    withoutspaces = withspaces.lstrip(' ')
                    row[4] = withoutspaces
                    # delete none digits from row[17]
                    digits = []
                    for i in row[17]:
                        if i.isdigit():
                            digits.append(i)
                    row[17] = ''.join(digits)

            # Add the updated row / list to the output file
            csv_writer.writerow(row)

    splitcsv(temp_file5, originalname, lspeedversion)


for file in os.listdir(current_path):
    filename = os.fsdecode(file)
    if filename.endswith(".csv") and not filename.startswith("for gls_"):
        filename_string = os.path.join(current_path, filename)
        print("Datei:", filename_string)
        # check if its an export and if yes than procced with converting
        with open(filename) as f:
            try:
                first_line = f.readline()
            except UnicodeDecodeError:
                print("kann nicht mal die erste zeile der csv datei lesen: ", filename)
                first_line = "not valid first row"
            if first_line[0:8] == 'Shipment':
                # the proccedere for shipment
                filenumber = filenumber + 1
                print("[++] file number: ", filenumber)
                lightespeedexportversion = 0
                print("shipment-csv proccedere!")
                sonderzeichenraus(filename)
                convert(validcsv(filename), lightespeedexportversion)
                rmtempfiles()
                print("[+] all done!")
            elif first_line[0:8] == 'Order_ID':
                # the proccedere for orders
                filenumber = filenumber + 1
                print("[++] file number: ", filenumber)
                lightespeedexportversion = 1
                print("orders-csv proccedere!")
                sonderzeichenraus(filename)
                convert(validcsv(filename), lightespeedexportversion)
                rmtempfiles()
                print("[+] all done!")
            else:
                print("Datei ist nicht von litghespeed")
        continue
    else:
        if filename.endswith(".csv"):
            print("keine litghespeed Datei:", filename)
        continue
