#!/usr/bin/env python3
import csv
import fileinput
import os


filename_for_proccedere = 'Sendungen_Export_201217.csv'
newfile1 = 'newtest1.csv'
newfile2 = 'newtest2.csv'

def getdelimeter(file):
    sample = open(file).read(2048)
    dialect = csv.Sniffer().sniff(sample)
    print("dialect: ", dialect.delimiter)
    return dialect.delimiter


# print("[+] print rows")
# row_count = 0  # Current amount of rows processed
# with open(filename_for_proccedere, "r", newline='', encoding='utf-8') as source:
#     reader = csv.reader(source, delimiter=getdelimeter(filename_for_proccedere))
#     with open(newfile1, "w", newline='', encoding='utf-8') as result:
#         writer = csv.writer(result)
#         for row in reader:
#             print("row: ", row)
#             writer.writerow(row)



print("[+] delete not nessecery columns")
cols_to_remove = [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 22, 24, 26, 28, 31, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60] # was neues
cols_to_remove = sorted(cols_to_remove, reverse=True)  # Reverse so we remove from the end first
row_count = 0  # Current amount of rows processed

with open(filename_for_proccedere, "r", encoding='utf-8') as source:
    reader = csv.reader(source, delimiter=getdelimeter(filename_for_proccedere))
    with open(newfile2, "w", newline='', encoding='utf-8') as result:
        writer = csv.writer(result, delimiter=',')
        for row in reader:
            row_count += 1
            # print('\r{0}'.format(row_count), end='')  # Print rows processed
            for col_index in cols_to_remove:
                #print("cols_to_remove: ", cols_to_remove)
                print("col_index: ", col_index)
                del row[col_index]
            writer.writerow(row)
